#!/bin/sh

set -e

PACKAGE=ganv
BASE_REL=$(dpkg-parsechangelog 2>/dev/null | sed -ne 's/Version: \([0-9]\)~.*/\1/p')
OLDDIR=${PWD}
GOS_DIR=${OLDDIR}/get-orig-source
SVN_COMMIT="svn log http://svn.drobilla.net/lad/trunk/${PACKAGE}/ -l 1 | sed -ne 's/r\([0-9]\+\).*/\1/p'"
REPACK_SUFFIX=~dfsg0

if [ -z ${BASE_REL} ]; then
	echo 'Please run this script from the sources root directory.'
	exit 1
fi

UNPACK_WAF=$PWD/debian/unpack_waf.sh

rm -rf ${GOS_DIR}
mkdir ${GOS_DIR} && cd ${GOS_DIR}
PUGL_SVN_COMMIT=$(eval "${SVN_COMMIT}")
svn export -r ${PUGL_SVN_COMMIT} http://svn.drobilla.net/lad/trunk/${PACKAGE} ${PACKAGE}
cd ${PACKAGE}/
${UNPACK_WAF}
cd .. && tar cjf \
	${OLDDIR}/${PACKAGE}_${BASE_REL}~svn${PUGL_SVN_COMMIT}${REPACK_SUFFIX}.orig.tar.bz2 \
	${PACKAGE} --exclude-vcs
rm -rf ${GOS_DIR}
